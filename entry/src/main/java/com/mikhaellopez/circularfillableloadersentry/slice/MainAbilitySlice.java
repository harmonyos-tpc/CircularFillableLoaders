package com.mikhaellopez.circularfillableloadersentry.slice;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.mikhaellopez.circularfillableloadersentry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private List<Integer> colors = new ArrayList<>();

    private CircularFillableLoaders circularFillableLoaders;
    private DiscreteSeekBar progress;
    private DiscreteSeekBar amplitude;
    private DiscreteSeekBar borderWidth;
    private DiscreteSeekBar shaderColor;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        colors.add(Color.GRAY.getValue());
        colors.add(0xffff4444);
        colors.add(0xff99cc00);
        colors.add(0xffaa66cc);
        colors.add(0xff33b5e5);
        colors.add(0xffffbb33);

        circularFillableLoaders = (CircularFillableLoaders)findComponentById(ResourceTable.Id_cfl);
        progress = (DiscreteSeekBar)findComponentById(ResourceTable.Id_progress);
        borderWidth = (DiscreteSeekBar)findComponentById(ResourceTable.Id_borderWidth);
        amplitude = (DiscreteSeekBar)findComponentById(ResourceTable.Id_amplitude);
        shaderColor = (DiscreteSeekBar)findComponentById(ResourceTable.Id_shaderColor);
        circularFillableLoaders.setProgress(progress.getProgress());
        circularFillableLoaders.setBorderWidth(borderWidth.getProgress() * 3);
        circularFillableLoaders.setAmplitudeRatio((float) amplitude.getProgress() / 1000);
        progress.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                circularFillableLoaders.setProgress(i);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        borderWidth.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                circularFillableLoaders.setBorderWidth(i * 3);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        amplitude.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                circularFillableLoaders.setAmplitudeRatio((float) i / 1000);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        int color = colors.get(0);
        shaderColor.setThumbColor(color, color, color);
        shaderColor.setScrubberColor(color);
        circularFillableLoaders.setColor(color);

        shaderColor.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                int color = colors.get(i);
                shaderColor.setThumbColor(color, color, color);
                shaderColor.setScrubberColor(color);
                circularFillableLoaders.setColor(color);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        circularFillableLoaders.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                circularFillableLoaders.setPixelMap(ResourceTable.Media_logo);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
