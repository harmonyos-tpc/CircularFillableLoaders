# demo

<img src="art/demo.gif" width="40%"/>


## Installation instructions:
Method 1:
For using circularfillableloaders module in sample app,add the below dependency to generate hap/har.
Modify entry/build.gradle as below:
```gradle
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile'junit:junit:4.12'
    compile project(path: ':circularfillableloaders')
}
```

Method 2:
For using circularfillableloaders in separate application using har file, generate .har package through library and add the har file to the entry/libs folder.
Modify entry/build.gradle as below :
```gradle
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```

Method 3:
For using circularfillableloaders in separate application using maven dependency,modify entry/build.gradle as below :
             
```gradle
   dependencies {
       implementation 'io.openharmony.tpc.thirdlib:CircularFillableLoaders:1.0.2'
   }
```

## Usage instructions:

**1)** Add the component to your layout XML.

```xml
<com.mikhaellopez.circularfillableloaders.CircularFillableLoaders
        ohos:id="$+id:cfl"
        ohos:height="match_content"
        ohos:width="match_content"
        app:cfl_img="$media:logo"
        app:cfl_progress="80"
        app:cfl_wave_color="#ff3f51b5"
        />
```

**2)** CircularFillableloader component usage in java is as below
```java
        circularFillableLoaders.setProgress(progress);
        circularFillableLoaders.setBorderWidth(borderWidth);
        circularFillableLoaders.setAmplitudeRatio(amplitude);
        circularFillableLoaders.setColor(color);

        circularFillableLoaders.setPixelMap(ResourceTable.Media_logo)
        circularFillableLoaders.setPixelMap(PixelMap)
```

# API:
## class CircularFillableLoaders
**public void setPixelMap(PixelMap pixelMap)**
- description: set image icon with pixelmap

**public void setPixelMap(int resId)**
- description: set image icon with media file res ID

**public void setPixelMap(Element element)**
- description: set iamge icon with element

**public void setColor(int color)**
- description: set wave color

**public void setBorderWidth(float width)**
- description: set border width

**public void setAmplitudeRatio(float amplitudeRatio)**
- description: set wave amplitude ratio

**public void setProgress(int progress)**
- description: set progress

**public void setProgress(int progress, int milliseconds)**
- description: set progress with duration

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| cfl_img | element | set image icon default null
| cfl_wave_color | color | set wave color default BALCK
| cfl_wave_amplitude | float | set wave amplitude default 0.05f
| cfl_progress | int | set progress default 0
| cfl_border | boolean | need border default ture
| cfl_border_width| dimension | set border width

